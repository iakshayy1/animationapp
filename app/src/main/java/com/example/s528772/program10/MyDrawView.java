package com.example.s528772.program10;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by S528772 on 12/2/2017.
 */

public class MyDrawView extends View implements View.OnClickListener,View.OnLongClickListener,View.OnTouchListener {
    private String mExampleString;
    private int mExampleColor = Color.RED;
    private float mExampleDimension = 0;
    private Drawable mExampleDrawable;

    private ArrayList<Point> points;
    private boolean drawFigure;
    private float lastx, lasty;

    private TextPaint mTextPaint;
    private float mTextWidth;
    private float mTextHeight;

    public MyDrawView(Context context) {
        super(context);
        init(null, 0);
    }

    public MyDrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public MyDrawView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        points = new ArrayList<Point>();
        setOnTouchListener(this);
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.MyDrawView, defStyle, 0);
        mExampleString = a.getString(
                R.styleable.MyDrawView_exampleString);
        mExampleColor = a.getColor(
                R.styleable.MyDrawView_exampleColor,
                mExampleColor);
        mExampleDimension = a.getDimension(
                R.styleable.MyDrawView_exampleDimension,
                mExampleDimension);
        if (a.hasValue(R.styleable.MyDrawView_exampleDrawable)) {
            mExampleDrawable = a.getDrawable(
                    R.styleable.MyDrawView_exampleDrawable);
            mExampleDrawable.setCallback(this);
        }

        a.recycle();

        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        invalidateTextPaintAndMeasurements();
    }

    private void invalidateTextPaintAndMeasurements() {
        mTextPaint.setTextSize(mExampleDimension);
        mTextPaint.setColor(mExampleColor);
        mTextWidth = mTextPaint.measureText(mExampleString);

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        mTextHeight = fontMetrics.bottom;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        Paint pen = new Paint();
        pen.setStrokeWidth(10.0f);
        pen.setStyle(Paint.Style.STROKE);
        pen.setColor(Color.MAGENTA);
        pen.setColor(Color.BLACK);

        pen.setTextSize(65.0f);
        pen.setColor(Color.RED);
        canvas.drawText("Hello World " + "", 400, 100, pen);

        pen.setStrokeWidth(5.0f);
        pen.setColor(Color.BLACK);

        canvas.drawRect(325, 450, 800, 700, pen);

        pen.setColor(Color.BLACK);
        canvas.drawCircle(510, 275, 20.0f, pen);
        canvas.drawCircle(590, 275, 20.0f, pen);
        canvas.drawCircle(550, 300, 150.0f, pen);
        pen.setStrokeWidth(5.0f);
        pen.setColor(Color.BLACK);

        pen.setStyle(Paint.Style.STROKE);
        pen.setColor(Color.BLACK);
        canvas.drawLine(550, 700, 375, 800, pen);
        canvas.drawLine(550, 700, 725, 800, pen);
        canvas.drawLine(725, 800, 375, 800, pen);
        pen.setColor(Color.GREEN);
        pen.setStyle(Paint.Style.STROKE);
        canvas.drawOval(550, 540, 570, 570, pen);
        canvas.drawOval(550, 590, 570, 620, pen);
        canvas.drawOval(550, 640, 570, 670, pen);
        pen.setColor(Color.MAGENTA);
        pen.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawLine(800, 500, 900, 800, pen);
        canvas.drawLine(325, 500, 225, 800, pen);
        canvas.drawArc(400, 200, 700, 400, 80, 20, false, pen);


        for(Point point: points )
            canvas.drawCircle(point.x, point.y, 20.0f, pen);
        }


    @Override
    public void onClick(View view) {
        if(drawFigure){
            points.clear();
            drawFigure = false;
        }
        else
        {
            points.add(new Point((int)lastx, (int)lasty));

        }
        invalidate();
    }

    @Override
    public boolean onLongClick(View view) {

        points.add(new Point((int)lastx, (int)lasty));
        points.add(points.get(0));
        drawFigure = true;
        invalidate();
        return true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                points.add(new Point((int) motionEvent.getX() , (int) motionEvent.getY() ));
                lastx = motionEvent.getX();
                lasty = motionEvent.getY();
                super.onTouchEvent(motionEvent);
                if(points.size() > 1)
                    drawFigure = true;
                break;
            case MotionEvent.ACTION_MOVE:
                Log.i("TAG", "moving: (" + lastx + ", " + lasty + ")");
                break;
            case MotionEvent.ACTION_UP:
                break;
        }

        invalidate();
        return true;

    }
}
